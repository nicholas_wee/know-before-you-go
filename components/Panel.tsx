import React, { Children } from 'react'
import style from '../styles/Panel.module.scss'

/**
 * Simple material design elevated panel
 */
export default function Panel({ children } : {
    children : React.ReactNode
}) {
    return (
        <div className={style.panel}>
            {children}
        </div>
    )
}
