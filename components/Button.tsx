import React from 'react'
import styles from "../styles/Button.module.scss"
import Ripple from './Ripple'

export default function Button({text, onClick = () => {}} : {text : string, onClick? : any}) {
    return ( 
        <div className={styles.buttonContainer} onClick={onClick}>
                <p>{text}</p>
        </div>
    )
}
