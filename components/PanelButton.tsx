import React from 'react'
import Panel from './Panel'
import Image from 'next/image'
import styles from '../styles/Panel.module.scss'
import { Secrets } from '../constants/secrets'
import Link from 'next/link'

/**
 * Enlarged panel button
 */
export default function PanelButton({ title, iconUrl, href }: {
    title: string,
    iconUrl: string,
    href: string
}) {

    
    return (
        <Panel>
            <div className={styles.panelButton}>
                <Link href={href}>
                    <div>
                        <img src={`${Secrets.S3_URL}${iconUrl}`} width={70} height={70} alt="icon" />
                        <h2>{title}</h2>
                    </div>

                </Link>

            </div>
        </Panel>
    )
}
