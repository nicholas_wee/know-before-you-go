import React from 'react'
import Link from 'next/link'
import Image from 'next/image'
import styles from '../styles/Navigation.module.scss'
import NavigationButton from './NavigationButton'
import { Destinations } from './../constants/strings';
import { Secrets } from '../constants/secrets'

/**
 * Root component for Aside Navigation Panel
 */
export default function NavigationPanel() {
    return (
        <div className={styles.container}>
            <div className={styles.logo} >
                <img src={`${Secrets.S3_URL}/assets/swinburne_online_logo.svg`} height={100} width={190} alt="swinburne logo" />
            </div>
            <div className={styles.dots}>
                <NavigationButton name="Home" dest={Destinations.HOME} />
                <NavigationButton name="Modules" dest={Destinations.MODULES} />
                <NavigationButton name="Maps" dest={Destinations.MAPS} />
                <NavigationButton name="Support" dest={Destinations.SUPPORT} />
            </div>
        </div>
    )
}
