import React, { ReactDOM } from 'react'
import Link from 'next/link';
import styles from '../styles/Navigation.module.scss';
import { useRouter } from 'next/router'

type ButtonProps = {
    dest: string,
    name: string
}

/**
 * Smart navigation button
 */
export default function NavigationButton(props: ButtonProps) {
    const router = useRouter()
    let { dest, name } = props;
    // Checks if current route is equal to the destination of component
    // router path name will return "<pathname>" but "/" if at index or home
    // Therefore sanitizing it to account for this discrepency
    const sanitizedPathname = router.pathname.replace("/", "")

    //if route is a subroute, amend routes for all buttons
    const pathnameBase = `../${sanitizedPathname.split("/")[0]}`
    if(sanitizedPathname.split("/").length===2){
        dest = dest.replace("/", "")
        dest = `../${dest}`
    }
    
    const isSameRoute = sanitizedPathname == dest || router.pathname == dest || pathnameBase == dest

    return (
        <Link href={dest}>
            <div className={`${styles.button} ${isSameRoute ? styles.selected : styles.unselected}`}>
                <strong>{name}</strong>
            </div>
        </Link>
    )
}
