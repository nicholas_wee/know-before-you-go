import React, { Component } from 'react'
import Faq from 'react-faq-component';
import style from '../styles/Dropdown.module.scss'

export default function Dropdown({ title, text }: {
    title: string,
    text: string
}) {

    const data = {
        rows: [
            {
            title: title,
            content: text
            }
        ]
    }

    return (
        <div className={style.dropdownBlock}>
          <Faq data={data}/>
        </div>
    )
}



