import React from 'react'
import styles from '../styles/Overlay.module.scss'
export default function Overlay() {
    return (
        <div className={styles.overlay}>
            <strong>KNOW BEFORE YOU GO</strong>
            <p>Nursing Onboarding Experience</p>
        </div>
    )
}
