import { createRipples } from 'react-ripples'
 
const CustomRipple = createRipples({
  color: "rgba(0, 0, 0, .1)",
  during: 1000,
})

import React from 'react'

export default function Ripple({children}) {
    return (
        <CustomRipple>
            <div style={{width: "100%"}}>
            {children}
            </div>
        </CustomRipple>
    )
}
