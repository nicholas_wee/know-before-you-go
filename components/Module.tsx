import React from 'react'
import Faq from 'react-faq-component';
import styles from "../styles/Modules.module.scss"
import Panel from './Panel';
import ItemType from '../interfaces/ItemType';
import ModuleType from '../interfaces/ModuleType';
import Ripple from './Ripple';
import Link from 'next/link';
import {LocalStorage} from '../constants/storage';
import useSWR from 'swr';
import moment from 'moment';

// TODO: Needs to be converted to a enviroment variable secret
const youtubeAPIKey = "AIzaSyC_LQ_f0GHz5egh1xQIY_YGNgM_uO5FFmo"
// https://www.googleapis.com/youtube/v3/videos?id=uwoMSsrmPb8&part=contentDetails&key=AIzaSyC_LQ_f0GHz5egh1xQIY_YGNgM_uO5FFmo

const fetcher = (url) => fetch(url).then((res) => res.json())

function GetDuration(videoId) {
    const {
        data,
        error
    } = useSWR(`https://www.googleapis.com/youtube/v3/videos?id=${videoId}&part=contentDetails&key=${youtubeAPIKey}`, fetcher)
    if (error) return null
    if (!data) return null
    return moment.duration(data.items[0].contentDetails.duration)
}

function fancyTimeFormat(duration) {
    // Hours, minutes and seconds
    var hrs = ~~(duration / 3600);
    var mins = ~~((duration % 3600) / 60);
    var secs = ~~duration % 60;

    // Output like "1:01" or "4:03:59" or "123:03:59"
    var ret = "";

    if (hrs > 0) {
        ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
    }

    ret += "" + mins + ":" + (secs < 10 ? "0" : "");
    ret += "" + secs;
    return ret;
}


function calculateTotalPercentage(items: Array<ItemType>, moduleId: number) {
    let totalPercentage = 0
    items.forEach((item, itemId) => {
        let progress = fetchModuleProgress(moduleId, itemId)
        let duration = GetDuration(item.videoId)
        if (duration != null) {
            let moduleProgress = progress / duration.asSeconds()
            totalPercentage += moduleProgress
        }
    });
    return Math.round(totalPercentage / items.length * 100)

}

function calculateTotalDuration(items: Array<ItemType>) {
    let totalTime = 0;
    items.forEach((item, itemId) => {
        let duration = GetDuration(item.videoId)
        if (duration != null) {
            totalTime += duration.asMinutes();
        }
        
    });
    return totalTime
}

function Title({title, thumbnail, items}: ModuleType, moduleId: number) {
    const totalItems = items.length;
    const totalTime = calculateTotalDuration(items);
    const percentage = calculateTotalPercentage(items, moduleId);

    
    

     

    return (<div className={styles.moduleContainer}>
            <div className={styles.moduleThumbnail}>
                <img src={thumbnail}/>
            </div>
            <div>
                <h1 className={styles.moduleTitle}>{title}</h1>
                <div className={styles.moduleSubtitleContainer}>
                    <p className={styles.moduleSubtitle}>{Math.round(totalTime)} {totalTime > 1 ? "minutes" : "minute"}, {totalItems} {totalItems > 1 ? "lessons" : "lesson"}</p>
                    <p className={styles.modulePercentage}>{percentage}%</p>
                </div>
            </div>
        </div>
    )
}

function Items(items: Array<ItemType> = [], moduleId: number) {
    return (
        <div className={styles.itemContainer}>
            {
                items.map((item, i) => <Item key={i} itemData={item} moduleId={moduleId} itemId={i}/>)
            }
        </div>
    )
}

function fetchModuleProgress(moduleId: number, itemId: number) {
    var moduleProgressData = {}
    if (typeof window !== 'undefined') try {
        moduleProgressData = JSON.parse(localStorage.getItem(LocalStorage.VIDEO_PROGRESS));
    } catch {
    }
    var progressTime = 0
    try {
        progressTime = moduleProgressData[moduleId][itemId]
    } catch {
    }
    if (progressTime == null) progressTime = 0
    return progressTime;
}

function calculatePercentage(progressTime: number, duration: moment.Duration) {
    return Math.round(progressTime / duration.asSeconds() * 100)
}

function Item({itemData, moduleId, itemId}: { itemData: ItemType, moduleId: number, itemId: number }) {
    var {title, videoId} = itemData;
    var duration = GetDuration(videoId);
    var formattedDateTime = "";

    if (duration != null) {
        formattedDateTime = fancyTimeFormat(duration.asSeconds());
        const progressTime = fetchModuleProgress(moduleId, itemId);
        var progressPercentage = calculatePercentage(progressTime, duration)
    }

    return (
        <Ripple>
            <Link href={{
                pathname: "/Modules/ModuleViewer",
                query: {moduleId: moduleId, itemId: itemId}
            }}>
                <div className={styles.itemCard}>
                    <div>
                        <div className={styles.itemTitle}>
                            <p>{title}</p>
                        </div>
                        <div className={styles.itemSubTitle}>
                            <p>{formattedDateTime}</p>
                        </div>
                    </div>
                    <div className={styles.itemInteractablesContainer}>
                        <p className={styles.itemPercentage}>{progressPercentage}%</p>
                        <div className={styles.itemPlayButton}>
                            ▲
                        </div>
                    </div>
                </div>
            </Link>
        </Ripple>
    )
}

function ModuleItem({module, moduleId}: { module: ModuleType, moduleId: number }) {
    const data = {
        rows: [
            {
                title: Title(module, moduleId),
                content: Items(module.items, moduleId)
            }
        ]
    }

    return (
        <Panel>
            <div className={styles.moduleFAQ}>
                <Faq data={data}/>
            </div>
        </Panel>
    )
}

export default function Module({modules = []}: { modules: Array<ModuleType> }) {
    return <div className={styles.modulesContainer}>
        {
            modules.map((m, i) => <ModuleItem key={i} module={m} moduleId={i}/>)
        }
    </div>
}
