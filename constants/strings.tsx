

/**
 * Locations for navigations
 */
export class Destinations {
    static HOME = "/"
    static MAPS = "/Maps"
    static MODULES = "/Modules"
    static SUPPORT = "/Support"
}
