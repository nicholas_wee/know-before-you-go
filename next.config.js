module.exports = {
  // Load image from CloudFront URL
  // Ref: https://nextjs.org/docs/api-reference/next/image#domains
  images: {
    domains: ['d1plzxx184bbhj.cloudfront.net'],
  },
  reactStrictMode: true,
  ignoreBuildErrors: true
}
