import Head from 'next/head'
import Image from 'next/image'
import Panel from '../components/Panel'
import PanelButton from '../components/PanelButton'
import styles from '../styles/Home.module.scss'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Know Before You Go | Nursing Onboarding Experience</title>
        <link rel="icon" href="/favicon.png" />
      </Head>
      <h1 className="title">Welcome!</h1>
      <p className="subtitle">Get a head start before you embark.</p>
      <br />
      <Panel>
        <div className={styles.splash} />
      </Panel>
      <br />
      <div className={styles.options}>
        <PanelButton title="Quick Start" iconUrl="/assets/flag_black_24dp.svg" href = "documents/user_manual.pdf" />
        <PanelButton title="Resume" iconUrl="/assets/not_started_black_24dp.svg" href = "/Modules" />
        <PanelButton title="Notifications" iconUrl="/assets/notifications_black_24dp.svg" href = ""/>
      </div>
    </div>
  )
}
