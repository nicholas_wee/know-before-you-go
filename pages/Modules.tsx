import React from 'react'
import Module from '../components/Module';
import ModuleType from '../interfaces/ModuleType';
import moduleData from "../data/modules.json"

const modules : Array<ModuleType> = moduleData

// const exampleItems: Array<ItemType> = [
//     { title: "test", time: 100000, percentage: 10 },
//     { title: "test", time: 100000, percentage: 10 },
//     { title: "test", time: 100000, percentage: 10 },
//     { title: "test", time: 100000, percentage: 10 },
//     { title: "test", time: 100000, percentage: 10 }
// ]

// const exampleModules: Array<ModuleType> = [
//     { title: "Module 1: Site Introduction", thumbnail: "https://i.imgur.com/YDwdzFQ.png", items: [
//         { title: "Campus Introduction", time: 74400, percentage: 25 },
//         { title: "Covid Safe Practices", time: 63600, percentage: 0 },
//         { title: "Basic Hygiene", time: 264000, percentage: 0 },
//         { title: "Performing Tasks", time: 150000, percentage: 0 },
//     ] },
//     { title: "Module 2: Lab Procedures", thumbnail: "https://i.imgur.com/uoUapSe.png", items: [
//         { title: "test", time: new Date(0,0, 0,0, 1, 24).getMilliseconds(), percentage: 10 }
//     ] },
//     { title: "Module 3: Instrumentation", thumbnail: "https://i.imgur.com/efR1yG4.png", items: [
//         { title: "test", time: new Date(0,0, 0,0, 1, 24).getMilliseconds(), percentage: 10 }
//     ] },
// ]
export default function Modules(){
    return (
        <div>
            <h1 className="title">Explore Modules</h1>
            <p className="subtitle" style={{paddingBottom: 30}}>Click on any module to begin.</p>

            <Module modules={modules}/>
        </div>
    )
}
