import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import YouTube, { Options } from 'react-youtube'
import Button from '../../components/Button'
import Panel from '../../components/Panel'
import { Destinations } from '../../constants/strings'
import moduleData from "../../data/modules.json"
import ModuleType from '../../interfaces/ModuleType'
import styles from '../../styles/Modules.module.scss'
import { YouTubePlayer } from 'youtube-player/dist/types';
import { LocalStorage } from '../../constants/storage'

const modules: Array<ModuleType> = moduleData

// This is the interval that it'll record/save video progress time at
// eg. 5000 will save the current videos time every 5 seconds
const saveTimeInterval = 100

export default function ModuleViewer() {
    const { query } = useRouter()
    const { moduleId, itemId } = query
    const [player, setPlayer] = useState<YouTubePlayer>()

    var moduleProgressData = {}
    if (typeof window !== 'undefined') try { moduleProgressData = JSON.parse(localStorage.getItem(LocalStorage.VIDEO_PROGRESS)); } catch { }

    // Sets up persistant local progress time listener
    useEffect(() => {
        // On Load/Start
        if (moduleId != undefined) {
            const moduleIdNum = parseInt(moduleId as string)
            const itemIdNum = parseInt(itemId as string)

            // Persists progress data to local storage
            const saveTime = (time: number) => {
                if (moduleProgressData == null) moduleProgressData = {}
                moduleProgressData[moduleIdNum] = moduleProgressData[moduleIdNum] || {};
                moduleProgressData[moduleIdNum][itemIdNum] = time
                localStorage.setItem(LocalStorage.VIDEO_PROGRESS, JSON.stringify(moduleProgressData));
            }

            const interval = setInterval(() => {
                if (player != null) {
                    if (player.getPlayerState() == YouTube.PlayerState.PLAYING)
                        saveTime(player.getCurrentTime())
                }
            }, saveTimeInterval)
            
            // Clean up intervals on destroy
            return () => clearInterval(interval);
        }
    })

    // This check is required due to NextJS's hydration
    // Page will load once without router URL params and then another after with params
    if (moduleId != undefined) {
        const moduleIdNum = parseInt(moduleId as string)
        const itemIdNum = parseInt(itemId as string)

        const module = modules[moduleIdNum]
        const item = module.items[itemIdNum]
        console.log(moduleIdNum + "and" + itemIdNum)

        const opts: Options = {
            height: '672',
            width: '1197',
            playerVars: {
                autoplay: 0,
            },
        }

        var nextVisibility = true;
        var backVisibility = true;

        var backModuleId = moduleIdNum;
        var backItemId = itemIdNum - 1;

        var nextModuleId = moduleIdNum;
        var nextItemId = itemIdNum + 1;

        // Next Logic
        if (moduleIdNum >= modules.length - 1 && itemIdNum >= module.items.length -1) {
            nextVisibility = false;
        } else if (itemIdNum >= module.items.length - 1) {
            nextModuleId++;
            nextItemId = 0;
        }

        // Back Logic
        if (moduleIdNum <= 0 && itemIdNum <= 0) {
            backVisibility = false;
        } else if (itemIdNum <= 0) {
            backModuleId--;
            backItemId = modules[moduleIdNum - 1].items.length - 1;
        }

        // Sets the video time to stored progress time (if present) when video is initialized
        // Unsure if this is required? Youtube already stores data on video progress
        const onReady = (event) => {
            const player: YouTubePlayer = event.target
            try { player.seekTo(moduleProgressData[moduleIdNum][itemIdNum]) } catch { }
            setPlayer(player)
        }

        return (
            <div>
                <h1 className="title">{item.title}</h1>
                <p className="subtitle" style={{ paddingBottom: 30 }}>{item.description}</p>
                <div className={styles.moduleVideo}>
                    <YouTube
                        onReady={onReady}
                        videoId={item.videoId}
                        opts={opts}
                    />
                </div>

                <div className={styles.moduleVideoButtonContainer}>
                    <div style={backVisibility ? {} : { visibility: "hidden" }}>
                        <Link href={{ pathname: `${Destinations.MODULES}/ModuleViewer`, query: { moduleId: backModuleId, itemId: backItemId } }}>
                            <Button text="Back" />
                        </Link>
                    </div>
                    <Link href={`${Destinations.MODULES}`}>
                        <div className={styles.moduleInfoContainer}>
                            <p><strong>{module.title}</strong> [Video {itemIdNum + 1}]</p>
                        </div>
                    </Link>
                    <div style={nextVisibility ? {} : { visibility: "hidden" }}>
                        <Link href={{ pathname: `${Destinations.MODULES}/ModuleViewer`, query: { moduleId: nextModuleId, itemId: nextItemId } }}>
                            <Button text="Next" />
                        </Link>
                    </div>
                </div>
            </div>
        )
    } else {
        return <div></div>
    }
}
