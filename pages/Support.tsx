import React from 'react'
import NavigationPanel from '../components/NavigationPanel'
import Panel from './../components/Panel';
import style from '../styles/Support.module.scss'
import Dropdown from '../components/Dropdown';
import PanelButton from '../components/PanelButton';

export default function Support() {
    return (
        <div>

            <h1 className="title">Support</h1>
            <p className="subtitle">Stuck? Check these out.</p>
            <div className={style.supportContainer}>
                <Panel>
                    <h1>FAQ</h1>
                    <Dropdown title="How do I access the Simulation Lab?" text="The Simulation Lab is located in the West wing of Swinburne's Wantirna Campus. 
                    For more accurate directions, you can refer to the map in the Maps tab."/>
                    <Dropdown title="Is my progress in the module saved?" text="Your progress in each module is saved, and may be monitored by your tutors and 
                    supervisors so that they can track your learning progress."/>

                    <h1>Troubleshooting</h1>
                    <Dropdown title="How can I reduce buffering?" text="Try to reduce the resolution in the 360° panel, and lower the video resolution. 
                    If that does not work, a faster internet connection may be required."/>
                </Panel>



                <div className={style.resourcesAndContactsContainer}>

                    <div className={style.resourcesContainer}>
                        <Panel>
                        <h1>Resources</h1>
                            <div className={style.height}>
                                <div className={style.resourcesSegment}>
                                    <div className={style.resourcesBlock}>
                                        <a href="https://www.youtube.com/watch?v=Lu6wZiX9g8U" target="_blank" rel="noreferrer"><img src="images/video_icon.png" width="80px" alt="Getting Started" /></a>
                                        <p>Getting Started</p>
                                    </div>
                                    <div className={style.resourcesBlock}>
                                        <a href="documents/student-handbook.pdf"><img src="images/pdf_logo.png" width="80px" alt="Student Handbook" /></a>
                                        <p>Student Handbook</p>
                                    </div>
                                    <div className={style.resourcesBlock}>
                                        <a href="documents/user_manual.pdf" target="_blank" rel="noreferrer"><img src="images/pdf_logo.png" width="80px" alt="Student Handbook" /></a>
                                        <p>Website User Manual</p>
                                    </div>
                                </div>
                            </div>
                        </Panel>
                    </div>


                    <div className={style.contactsContainer}>
                        <Panel>
                        <h1>Important Contacts</h1>
                            <div>
                                
                                <p><b>I.T Support: </b>+61 469420690 (Weekdays, 8am-5pm)</p>
                                <p><b>Claims and tickets: </b><a href="https://swinburne.edu.au/claims-tickets">https://swinburne.edu.au/claims-tickets</a></p>
                                <p><b>Platform Bugs: </b><a href="mailto: kbyg@swin.edu.au">kbyg@swin.edu.au</a></p>
                            </div>
                        </Panel>
                    </div>





                </div>
                <div className={style.sponsorsContainer}>
                    
                    <Panel>
                        <h1>Sponsors</h1>
                        <div className={style.panelParent}>
                        <img className={style.swinburneOnlineLogo}src="images/swinburne_online_logo.png" alt="Online Education Services Logo" height="160px" />
                        <img src="images/oes_logo.png" alt="Online Education Services Logo" height="160px" />
                        <img src="images/cic_logo.png" alt="Online Education Services Logo" height="160px" />
                        </div>
                    </Panel>
                    
                    
                </div>

            </div>


        </div>
    )
}
