import React from "react";
import NavigationPanel from "../components/NavigationPanel";
import Panel from "../components/Panel";
import styles from "../styles/Maps.module.scss";
import Link from "next/link";

export default function Maps() {
    return (
        <div>
            <div>
                <h1 className="title">Explore Maps</h1>
                <p className="subtitle">Find your way around campus.</p>
            </div>
            <br />
            <br />
            <br />
            <br />
            <div className={styles.panelContainerLeft}>
                <Link href="/Maps/MapViewer?map=east">
                    <div className={styles.button}>
                        <Panel>
                            <img className={styles.image} src="/images/simlabeast.png" alt="Simulation Lab East" />
                            <h2 className={styles.title}>Simulation Lab East</h2>
                        </Panel>
                    </div>
                </Link>
            </div>
            <div className={styles.panelContainerRight}>
                <Link href="/Maps/MapViewer?map=west">
                    <div className={styles.button}>
                        <Panel>
                            <img className={styles.image} src="/images/simlabwest.png" alt="Simulation Lab West" />
                            <h2 className={styles.title}>Simulation Lab West</h2>
                        </Panel>
                    </div>
                </Link>
            </div>
        </div>
    );
}
