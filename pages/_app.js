import '../styles/globals.scss'
import layout from '../styles/Layout.module.scss'
import NavigationPanel from './../components/NavigationPanel';
import { Head } from 'next/head';
import Overlay from './../components/Overlay';
import { useEffect } from 'react';
import TagManager from 'react-gtm-module';

/**
 * Template will appear on all pages, <Component {...pageProps}/> will return react router content
 */
function MyApp({ Component, pageProps }) {

    useEffect(() => {
        TagManager.initialize({ gtmId: 'GTM-WL4C6BV' });
    }, []);

    return (<div>
        <Overlay />
        <div className={layout.containerGrid}>
            <aside className={layout.sidebar}>
                <NavigationPanel />
            </aside>

            <div className={layout.content}>
                <Component {...pageProps} />
            </div>
        </div>
    </div>)
}

export default MyApp
