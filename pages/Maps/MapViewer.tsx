import React from "react";
import { useRouter } from "next/router";
import styles from "../../styles/Maps.module.scss";
import { Secrets } from '../../constants/secrets';

export default function MapViewer() {
    const { query } = useRouter();

    return (
        <div>
            <div>
                <h1 className="title">Explore Maps</h1>
                <p className="subtitle">Find your way around campus.</p>
            </div>
            <br />
            <iframe

                className={styles.iframe}
                // Load Marzipano module from CloudFront based on query
                src={`${Secrets.S3_URL}/nursing-sim-lab/${query.map}.html`}
                allow="fullscreen"
                loading="lazy"
            ></iframe>
        </div>
    );
}
