export default interface ItemType {
    title: string,
    description: string,
    videoId: string,
    currentTime?: number,
}