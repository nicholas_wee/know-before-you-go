import ItemType from "./ItemType";

export default interface ModuleType {
    title: string
    thumbnail: string
    items: Array<ItemType>
}